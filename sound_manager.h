/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __64338aae45354212a2c5850ba64d3746__
#define __64338aae45354212a2c5850ba64d3746__

#include "engine/sound_manager.h"
#include "sound_buffer.h"
#include "sound_source.h"
#include <memory>

class SoundManager
{
public:
	static inline SoundSource createSource() { return SoundSource(manager()->createSource()); }
	static inline SoundBuffer createBuffer() { return SoundBuffer(manager()->createBuffer()); }

private:
	static std::unique_ptr<ISoundManager> m_Manager;

	inline SoundManager() {}
	inline ~SoundManager() {}

	static ISoundManager * manager();

	SoundManager(const SoundManager &) = delete;
	SoundManager & operator=(const SoundManager &) = delete;
};

#endif
