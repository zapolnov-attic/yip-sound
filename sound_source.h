/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __5dde8317d6b3e44fed1b407c8fc63779__
#define __5dde8317d6b3e44fed1b407c8fc63779__

#include "engine/sound_source.h"
#include "engine/dummy_sound_source.h"
#include <yip-imports/cxx-util/macros.h>
#include <memory>

class SoundSource
{
public:
	inline SoundSource() noexcept : m_Source(&DummySoundSource::instance()) {}
	inline SoundSource(ISoundSource * src) noexcept : m_Source(src->retain()) {}
	inline SoundSource(SoundSource && src) noexcept : m_Source(src.m_Source)
		{ src.m_Source = &DummySoundSource::instance(); }
	inline SoundSource(const SoundSource & src) noexcept : m_Source(src.m_Source->retain()) {}
	inline ~SoundSource() { m_Source->release(); }

	inline SoundSource & operator=(SoundSource && src)
		{ std::swap(m_Source, src.m_Source); return *this; }
	inline SoundSource & operator=(const SoundSource & src)
		{ src.m_Source->retain(); m_Source->release(); m_Source = src.m_Source; return *this; }

private:
	ISoundSource * m_Source;
};

#endif
