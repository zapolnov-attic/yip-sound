/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __9a9efd81587f640b071b07b1be98a676__
#define __9a9efd81587f640b071b07b1be98a676__

#include "engine/sound_buffer.h"
#include "engine/dummy_sound_buffer.h"
#include <yip-imports/cxx-util/macros.h>
#include <memory>

class SoundBuffer
{
public:
	inline SoundBuffer() noexcept : m_Buffer(&DummySoundBuffer::instance()) {}
	inline SoundBuffer(ISoundBuffer * src) noexcept : m_Buffer(src->retain()) {}
	inline SoundBuffer(SoundBuffer && src) noexcept : m_Buffer(src.m_Buffer)
		{ src.m_Buffer = &DummySoundBuffer::instance(); }
	inline SoundBuffer(const SoundBuffer & src) noexcept : m_Buffer(src.m_Buffer->retain()) {}
	inline ~SoundBuffer() { m_Buffer->release(); }

	inline SoundBuffer & operator=(SoundBuffer && src)
		{ std::swap(m_Buffer, src.m_Buffer); return *this; }
	inline SoundBuffer & operator=(const SoundBuffer & src)
		{ src.m_Buffer->retain(); m_Buffer->release(); m_Buffer = src.m_Buffer; return *this; }

	inline void setData(SoundFormat format, const void * data, size_t size, size_t frequencyHz)
		{ m_Buffer->setData(format, data, size, frequencyHz); }

private:
	ISoundBuffer * m_Buffer;
};

#endif
