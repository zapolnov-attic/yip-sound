#include "sound_source.h"

void ISoundSource::release()
{
	if (--m_RefCount <= 0)
		deleteThis();
}
