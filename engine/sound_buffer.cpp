#include "sound_buffer.h"

void ISoundBuffer::release()
{
	if (--m_RefCount <= 0)
		deleteThis();
}
