/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "openal_sound_manager.h"
#include "openal_sound_buffer.h"
#include "openal_sound_source.h"
#include <iostream>

#ifdef __APPLE__
 #include <TargetConditionals.h>
 #if TARGET_OS_IPHONE
  #include "ios/audio_interruption_listener.h"
 #endif
#endif

OpenALSoundManager::OpenALSoundManager()
	: m_Device(nullptr),
	  m_Context(nullptr),
	  m_IsBound(false)
{
  #if defined(__APPLE__) && TARGET_OS_IPHONE
	AudioInterruptionListener_setup();
  #endif

	const char * defaultDevice = alcGetString(nullptr, ALC_DEFAULT_DEVICE_SPECIFIER);
	std::clog << "Sound: using default device: " << (defaultDevice ? defaultDevice : "(null)") << std::endl;

	m_Device = alcOpenDevice(defaultDevice);
	if (UNLIKELY(!m_Device))
	{
		std::clog << "Sound: unable to open audio device." << std::endl;
		return;
	}

	m_Context = alcCreateContext(m_Device, nullptr);
	if (UNLIKELY(!m_Context))
	{
		std::clog << "Sound: unable to create OpenAL context." << std::endl;
		return;
	}

	bind();

	std::clog << "Sound: OpenAL vendor: " << alGetString(AL_VENDOR) << std::endl;
	std::clog << "Sound: OpenAL renderer: " << alGetString(AL_RENDERER) << std::endl;
	std::clog << "Sound: OpenAL version: " << alGetString(AL_VERSION) << std::endl;

  #if defined(__APPLE__) && TARGET_OS_IPHONE
	AudioInterruptionListener_setCallback([this](bool activate) { if (activate) bind(); else unbind(); });
  #endif
}

OpenALSoundManager::~OpenALSoundManager()
{
  #if defined(__APPLE__) && TARGET_OS_IPHONE
	AudioInterruptionListener_setCallback(std::function<void(bool)>());
  #endif

	bind();

	while (!m_Sources.empty())
		delete *m_Sources.begin();

	unbind();

	if (m_Context)
	{
		alcDestroyContext(m_Context);
		m_Context = nullptr;
	}

	if (m_Device)
	{
		alcCloseDevice(m_Device);
		m_Device = nullptr;
	}
}

bool OpenALSoundManager::bind()
{
	if (UNLIKELY(!alcMakeContextCurrent(m_Context)))
	{
		std::clog << "Sound: unable to activate OpenAL context." << std::endl;
		m_IsBound = false;
		return false;
	}

	m_IsBound = true;

	while (alGetError() != AL_NO_ERROR)
		;

	if (!m_DeletedSources.empty())
	{
		alDeleteSources(ALsizei(m_DeletedSources.size()), m_DeletedSources.data());
		checkError(__FILE__, __LINE__ - 1, "alDeleteSources");
		m_DeletedSources.clear();
	}

	if (!m_DeletedBuffers.empty())
	{
		alDeleteBuffers(ALsizei(m_DeletedBuffers.size()), m_DeletedBuffers.data());
		checkError(__FILE__, __LINE__ - 1, "alDeleteBuffers");
		m_DeletedBuffers.clear();
	}

	for (auto it : m_PendingBuffers)
		it->upload(it->m_Format, it->m_Data.data(), it->m_Data.size(), it->m_Frequency);
	m_PendingBuffers.clear();

	return true;
}

void OpenALSoundManager::unbind()
{
	if (UNLIKELY(!alcMakeContextCurrent(nullptr)))
		std::clog << "Sound: unable to deactivate OpenAL context." << std::endl;
	m_IsBound = false;
}

ISoundSource * OpenALSoundManager::createSource()
{
	std::unique_ptr<OpenALSoundSource> src(new OpenALSoundSource(this));
	m_Sources.insert(src.get());
	return src.release();
}

ISoundBuffer * OpenALSoundManager::createBuffer()
{
	std::unique_ptr<OpenALSoundBuffer> src(new OpenALSoundBuffer(this));
	m_Buffers.insert(src.get());
	return src.release();
}

void OpenALSoundManager::checkError(const char * file, int line, const char * func)
{
	for (;;)
	{
		ALenum error = alGetError();
		if (LIKELY(error == AL_NO_ERROR))
			return;

		std::cerr << "Sound: " << func << ": " << alGetString(error) << " (in file " << file << " at line "
			<< line << ")." << std::endl;
	}
}

ISoundManager * ISoundManager::create()
{
	return new OpenALSoundManager();
}
