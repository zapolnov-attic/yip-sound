/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "opensles_sound_manager.h"
#include "opensles_sound_source.h"
#include <iostream>

OpenSLESSoundManager::OpenSLESSoundManager()
	: m_EngineObject(nullptr),
	  m_Engine(nullptr),
	  m_OutputMixerObject(nullptr)
{
	const SLInterfaceID pIDs[1] = { SL_IID_ENGINE };
	const SLboolean pIDsRequired[1]  = { SL_BOOLEAN_TRUE };

	SLresult result = slCreateEngine(&m_EngineObject, 0, nullptr, 1, pIDs, pIDsRequired);
	if (UNLIKELY(result != SL_RESULT_SUCCESS))
	{
		std::cerr << "Sound: unable to create sound engine." << std::endl;
		return;
	}

	result = (*m_EngineObject)->Realize(m_EngineObject, SL_BOOLEAN_FALSE);
	if (UNLIKELY(result != SL_RESULT_SUCCESS))
	{
		std::cerr << "Sound: unable to initialize sound engine." << std::endl;
		return;
	}

	result = (*m_EngineObject)->GetInterface(m_EngineObject, SL_IID_ENGINE, &m_Engine);
	if (UNLIKELY(result != SL_RESULT_SUCCESS))
	{
		std::cerr << "Sound: unable to retrieve an instance of SL_IID_ENGINE interface." << std::endl;
		return;
	}

	const SLInterfaceID pOutputMixIDs[] = {};
	const SLboolean pOutputMixRequired[] = {};

	result = (*m_Engine)->CreateOutputMix(m_Engine, &m_OutputMixerObject, 0, pOutputMixIDs, pOutputMixRequired);
	if (UNLIKELY(result != SL_RESULT_SUCCESS))
	{
		std::cerr << "Sound: unable to create output mixer." << std::endl;
		return;
	}

	result = (*m_OutputMixerObject)->Realize(m_OutputMixerObject, SL_BOOLEAN_FALSE);
	if (UNLIKELY(!result != SL_RESULT_SUCCESS))
	{
		std::cerr << "Sound: unable to initialize output mixer." << std::endl;
		return;
	}
}

OpenSLESSoundManager::~OpenSLESSoundManager()
{
	if (m_OutputMixerObject)
	{
		(*m_OutputMixerObject)->Destroy(m_OutputMixerObject);
		m_OutputMixerObject = nullptr;
	}

	if (m_EngineObject)
	{
		(*m_EngineObject)->Destroy(m_EngineObject);
		m_Engine = nullptr;
		m_EngineObject = nullptr;
	}
}

ISoundSource * OpenSLESSoundManager::createSource()
{
	std::unique_ptr<OpenSLESSoundSource> src(new OpenSLESSoundSource(this));
	m_Sources.insert(src.get());
	return src.release();
}

ISoundBuffer * OpenSLESSoundManager::createBuffer()
{
/*
	std::unique_ptr<OpenSLESSoundBuffer> src(new OpenSLESSoundBuffer(this));
	m_Buffers.insert(src.get());
	return src.release();
*/
	return 0;
}

ISoundManager * ISoundManager::create()
{
	return new OpenSLESSoundManager();
}
