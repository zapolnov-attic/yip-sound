/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __16f834ae197a47f41acb0b1e712ab99f__
#define __16f834ae197a47f41acb0b1e712ab99f__

#include "openal_sound_manager.h"
#include "sound_buffer.h"
#include <vector>

#ifdef __APPLE__
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#endif

class OpenALSoundBuffer : public ISoundBuffer
{
public:
	OpenALSoundBuffer(OpenALSoundManager * manager);
	~OpenALSoundBuffer();

	void setData(SoundFormat format, const void * data, size_t size, size_t frequencyHz);

private:
	OpenALSoundManager * m_Manager;
	ALuint m_Handle;
	SoundFormat m_Format;
	std::vector<char> m_Data;
	size_t m_Frequency;

	bool ensureCreated();
	void destroy();

	void upload(SoundFormat format, const void * data, size_t size, size_t frequencyHz);

	void deleteThis();

	friend class OpenALSoundManager;
};

#endif
