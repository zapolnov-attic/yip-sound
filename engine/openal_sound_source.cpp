/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "openal_sound_source.h"

OpenALSoundSource::OpenALSoundSource(OpenALSoundManager * manager)
	: m_Manager(manager),
	  m_Handle(0)
{
	ensureCreated();
}

OpenALSoundSource::~OpenALSoundSource()
{
	destroy();
}

bool OpenALSoundSource::ensureCreated()
{
	if (!m_Manager || !m_Manager->isBound())
		return false;

	if (!m_Handle)
	{
		alGenSources(1, &m_Handle);
		OpenALSoundManager::checkError(__FILE__, __LINE__ - 1, "alGenSources");
	}

	return true;
}

void OpenALSoundSource::destroy()
{
	if (!m_Manager || !m_Handle)
		return;

	m_Manager->m_Sources.erase(this);

	if (!m_Manager->isBound())
		m_Manager->m_DeletedSources.push_back(m_Handle);
	else
	{
		alDeleteSources(1, &m_Handle);
		OpenALSoundManager::checkError(__FILE__, __LINE__ - 1, "alDeleteSources");
		m_Handle = 0;
		m_Manager = nullptr;
	}
}

void OpenALSoundSource::deleteThis()
{
	delete this;
}
