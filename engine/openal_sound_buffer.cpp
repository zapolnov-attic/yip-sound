/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "openal_sound_buffer.h"
#include <iostream>

OpenALSoundBuffer::OpenALSoundBuffer(OpenALSoundManager * manager)
	: m_Manager(manager),
	  m_Handle(0)
{
	ensureCreated();
}

OpenALSoundBuffer::~OpenALSoundBuffer()
{
	destroy();
}

void OpenALSoundBuffer::setData(SoundFormat format, const void * data, size_t size, size_t frequencyHz)
{
	if (!m_Manager || !m_Handle)
	{
		std::cerr << "Sound: attempted to set data for invalid buffer." << std::endl;
		return;
	}

	if (m_Manager->isBound())
	{
		m_Data.clear();
		m_Data.shrink_to_fit();
		upload(format, data, size, frequencyHz);
	}
	else
	{
		m_Data.resize(size);
		m_Data.shrink_to_fit();
		memcpy(m_Data.data(), data, size);
		m_Format = format;
		m_Frequency = frequencyHz;
		m_Manager->m_PendingBuffers.insert(this);
	}
}

bool OpenALSoundBuffer::ensureCreated()
{
	if (!m_Manager || !m_Manager->isBound())
		return false;

	if (!m_Handle)
	{
		alGenBuffers(1, &m_Handle);
		OpenALSoundManager::checkError(__FILE__, __LINE__ - 1, "alGenBuffers");
	}

	return true;
}

void OpenALSoundBuffer::destroy()
{
	m_Data.clear();
	m_Data.shrink_to_fit();

	if (!m_Manager || !m_Handle)
		return;

	m_Manager->m_Buffers.erase(this);
	m_Manager->m_PendingBuffers.erase(this);

	if (!m_Manager->isBound())
		m_Manager->m_DeletedBuffers.push_back(m_Handle);
	else
	{
		alDeleteBuffers(1, &m_Handle);
		OpenALSoundManager::checkError(__FILE__, __LINE__ - 1, "alDeleteBuffers");
		m_Handle = 0;
		m_Manager = nullptr;
	}
}

void OpenALSoundBuffer::upload(SoundFormat format, const void * data, size_t size, size_t frequencyHz)
{
	ALenum fmt = 0;
	switch (format)
	{
	case SoundMono8: fmt = AL_FORMAT_MONO8; break;
	case SoundMono16: fmt = AL_FORMAT_MONO16; break;
	case SoundStereo8: fmt = AL_FORMAT_STEREO8; break;
	case SoundStereo16: fmt = AL_FORMAT_STEREO16; break;
	}

	alBufferData(m_Handle, fmt, data, ALsizei(size), ALsizei(frequencyHz));
	OpenALSoundManager::checkError(__FILE__, __LINE__ - 1, "alBufferData");
}

void OpenALSoundBuffer::deleteThis()
{
	delete this;
}
