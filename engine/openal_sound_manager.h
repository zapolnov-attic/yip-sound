/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __24f53444c7070fd81dad845cfbf5a2cb__
#define __24f53444c7070fd81dad845cfbf5a2cb__

#include "sound_manager.h"
#include <yip-imports/cxx-util/macros.h>
#include <vector>
#include <unordered_set>

#ifdef __APPLE__
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#endif

class OpenALSoundBuffer;
class OpenALSoundSource;

class OpenALSoundManager : public ISoundManager
{
public:
	OpenALSoundManager();
	~OpenALSoundManager();

	inline bool isBound() const noexcept { return m_IsBound; }
	bool bind();
	void unbind();

	ISoundSource * createSource();
	ISoundBuffer * createBuffer();

private:
	ALCdevice * m_Device;
	ALCcontext * m_Context;
	std::unordered_set<OpenALSoundSource *> m_Sources;
	std::vector<ALuint> m_DeletedSources;
	std::unordered_set<OpenALSoundBuffer *> m_Buffers;
	std::unordered_set<OpenALSoundBuffer *> m_PendingBuffers;
	std::vector<ALuint> m_DeletedBuffers;
	bool m_IsBound;

	static void checkError(const char * file, int line, const char * func);

	friend class OpenALSoundBuffer;
	friend class OpenALSoundSource;
};

#endif
